# Documento del PEC1: Un juego de aventuras de Pau Gallego

[JUGAR JUEGO AQUÍ](https://ghalek.itch.io/pec1-unjuegodeaventuras)

## Reglas del juego

El juego trata de insultar y responder a los insultos. Al principio de la partida se elige de forma aleatoria quien empieza el insulto y quien debe responder a dicho insulto para el primer asalto. El "insultador" debe elegir el insulto que quiera decir a su contrincante y el otro jugador, el "insultado", debe responder de forma adecuada al insulto escogido por el primero. Hay 16 insultos y 16 posibles respuestas.

Si el jugador que debe responder no logra responder de manera adecuada al insulto; este pierde el asalto, el insultador gana un asalto además de repetir su turno y volver a insultar.

Si logra responder de manera adecuada al insulto, gana el asalto y se tornan los turnos de manera que el insultado pasará a insultar y el insultador deberá responder al insulto en la siguiente ronda.

Gana el jugador que logre acumular 3 asaltos ganados.

# Desarrollo del juego

## Introducción

Para realizar este juego, se ha utilizado como plantilla de inicio un proyecto de Unity facilitado por el profesorado. En dicha plantilla venía incorporado una escena con un menú principal funcional y una escena con un pequeño juego cuya jugabilidad recuerda a un libro de "escoge tu aventura" además de un menú pausa funcional. El objetivo de este proyecto es crear un pequeño juego imitando las secuencias de duelos en el juego de Monkey Island en el que los contrincantes se insultan y se responden mutuamente. Dependiendo de la habilidad de cada contrincante para medir sus palabras y defenderse ante las del contrincante, uno saldrá victorioso y el otro será derrotado.

## Generación del primer turno

El primer turno, o sea el que insulta primero, se elige de manera aleatoria. Para esta función, se ha simulado el lanzamiento de una moneda debido a que al ser dos jugadores, la probabilidad es del 50% además de que el uso de monedas encaja bien con la estética pirata. Para ello se ha optado por una función que aleatoriza entre dos números.

```
        NumeroAleatorio entre 0 y 1:

        NumeroAleatorio = 0 (CARA)

        NumeroAleatorio = 1 (CRUZ)
```

Al principio de la partida el jugador podrá elegir el lado de la moneda que desee. El lado escogido se comprará con el resultado de la generación aleatoria anterior.

```
        NumeroAleatorio = 1 | MonedaJugador = 0       NumeroAleatorio = MonedaJugador = false       EMPIEZA EL CONTRINCANTE

        NumeroAleatorio = 0 | MonedaJugador = 0       NumeroAleatorio = MonedaJugador = true        EMPIEZA EL JUGADOR
```

## Los insultos y las respuestas

Al inicio del desarrollo se ha realizado un “script” en el que se definen y se redactan cada uno de los posibles insultos y respuestas que habrán en todo el juego. Estas frases se definen dentro de DOS matrices separadas. Una para insultos y otra para respuestas.

```
    public string[] insulto = new string[16];
    public string[] respuesta = new string[16];
```

Para después definir una a una la información de cada posición de las matrices.

```
insulto[0] = "¿Has dejado ya de usar pañales?";
insulto[1] = "...";
insulto[...] = "...";

respuesta[0] = "¿Por qué? ¿Acaso querías pedir uno prestado?";
respuesta[1] = "...";
respuesta[...] = "...";
```

El objetivo de definir las frases de esta forma es para poder comprar los textos de manera rápida dentro del bucle jugable. De manera que a la hora de determinar si las respuestas son adecuadas, simplemente tendrán que coincidir las posiciones de ambas matrices. Por ejemplo: el insulto de la posición 0 de la matriz solo puede ser contrarrestado con la respuesta de la posición 0 de la matriz de las respuestas.

```
insulto = 4 | respuesta = 5     x = y -> false      RESPUESTA INCORRECTA
insulto = 7 | respuesta = 7     x = y -> true       RESPUESTA CORRECTA
```

## El loop jugable

Una vez los insultos y las respuestas están definidas y se ha creado una función que permite comparar dichos textos. Dentro de la misma función se ha implementado las consecuencias de cada resultado.

```
SI la respuesta no es la adecuada al insulto:

        - [ ] El insultador gana un punto
        - [ ] Los roles se mantienen

SI la respuesta es adecuada al insulto:

        - [ ] El insultado gana un punto
        - [ ] Los roles se cambian
```

En este caso, debido a que el contrincante no es ningún jugador real, sus insultos y respuestas se generan de manera aleatoria. Para ello se ha creado una función exclusiva para generar un número de posición para la matriz de los insultos o respuestas dependiendo del turno del contrincante.

Dentro de la función "Update" hay una condición que comprueba constantemente si uno de los dos ha logrado alcanzar el límite de asaltos ganados. Cuando eso ocurre, comprueba quién de los dos ha ganado y lleva al jugador a la pantalla final correspondiente. Si ha ganado, le lleva a la pantalla de ganador y viceversa.

## Selección de opciones

La interfaz se ha diseñado de manera que el jugador pueda ver varias opciones pero sin que por ello vea todas a la vez. el jugador solo puede ver 5 de las 16 opciones pero puede escoger cualquiera. La interfaz de selección está distribuida tal que así:

```
            ...
Opción anterior a la anterior
Opciíon anterior
    OPCIÍON SELECCIONADA
Siguiente opciíon
Siguiente de la siguiente opción
            ...
```
Para poder cambiar y barajar entre las posibles opciones se han colocado botones con flechas en los extremos superior e inferior de la lista de opciones. Para dar con este resultado se ha codificado de manera similar al ejemplo siguiente:

```
respuesta[posicion-2]
respuesta[posicion-1]
respuesta[posicion]
respuesta[posicion+1]
respuesta[posicion+2]
```

Además de crear una condición que compruebe que si una de las 5 posiciones es inferior a 0 o superior a 15 se intercambian posiciones.

```
SI posicion < 0         posicion = 15
SI posicion > 15        posicion = 0
```

## Recursos utilizados

- [ ] Fuente utilizada: [Calson Antique](https://www.1001fonts.com/caslon-antique-font.html) de [Dieter Steffmann](https://www.1001fonts.com/users/steffmann/).
- [ ] Imagen de fondo: [Fondo](https://todoportadas.com/fondos-para-word/fondos-de-historia/).
- [ ] Botones y elementos de la interfaz: [Easy UI emerald - default](#https://assetstore.unity.com/packages/2d/gui/icons/easy-ui-emerald-default-112796) de [astr999](https://assetstore.unity.com/publishers/26585).
- [ ] Sonido de los botones: [Button click](https://freesound.org/people/Kolombooo/sounds/629020/) de [Kolombooo](https://freesound.org/people/Kolombooo/).
- [ ] Sonido de selección del insulto: [SwordClash09](https://freesound.org/people/32cheeseman32/sounds/180828/) de [32cheeseman32](https://freesound.org/people/32cheeseman32/).
- [ ] Musica de fondo: [The Pirate King](https://www.youtube.com/watch?v=iTVxFPhbAtk&list=PLsgdPlNfnCVlQXn5ElbCr00kjPbJ8yPlp&index=5) de [Untold Journey](https://www.youtube.com/@UntoldJourney).