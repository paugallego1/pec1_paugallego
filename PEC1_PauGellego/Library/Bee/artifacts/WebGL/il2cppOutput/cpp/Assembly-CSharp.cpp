﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtualFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
struct ExitGame_t7535BD4AC5611EBE2045397500E11C4CF600C05F;
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
struct Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872;
struct GoMainMenu_t210EECA25DD14A02EBB82B97AB3202433AAE8D0B;
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
struct NewGame_tEDA469253EA2F19368D26AC497B1B3A436945546;
struct PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF;
struct PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0;
struct Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8;
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
struct String_t;
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
struct UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC95F24D0C6E6B77389433852BB389F39C692926E;
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;

IL2CPP_EXTERN_C RuntimeClass* Application_tDB03BE91CDF0ACA614A5E0B67CFB77C44EB19B21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____9EFE357290EC4790277098DF1446E15073D2461F8C8BC387C3171F68AE0697EB_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____E5A4CAD744C5E475F71D0255D543D2077AA28BB76676E3DDE407EFC629AE60D3_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719;
IL2CPP_EXTERN_C String_t* _stringLiteral024B70546D560BED2F9EA3257D6EC71771696892;
IL2CPP_EXTERN_C String_t* _stringLiteral02B8F8D294374EB8249103C858B7C63EBB98F2F7;
IL2CPP_EXTERN_C String_t* _stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306;
IL2CPP_EXTERN_C String_t* _stringLiteral0B40411E1B54CCCB28E3B083FCBBF3EE2033F42D;
IL2CPP_EXTERN_C String_t* _stringLiteral149D2B7D3429AAA53F186CAD9F5D4ACD0C926F29;
IL2CPP_EXTERN_C String_t* _stringLiteral26DD1B6869931EE786AA1943B3DE335F28FE59D6;
IL2CPP_EXTERN_C String_t* _stringLiteral33D88DF242845191F062C3179CFA2E9D7A39A4CE;
IL2CPP_EXTERN_C String_t* _stringLiteral38D5603050B3CFF94274D7A379692AA384E561E3;
IL2CPP_EXTERN_C String_t* _stringLiteral3A0B2A280DA0A7150C032A71F485BD835B6C26A8;
IL2CPP_EXTERN_C String_t* _stringLiteral3F247E40A422873A44CCA84EA3443C5F50B7B9E1;
IL2CPP_EXTERN_C String_t* _stringLiteral432B2BC02EC7CFA29A59818E6A3EC3D1132278B2;
IL2CPP_EXTERN_C String_t* _stringLiteral4BA3F3E82F8AD9EC15073A10072D1BE61E0AAE82;
IL2CPP_EXTERN_C String_t* _stringLiteral4E16AB375A40CDDDEDE1C73E116812F8AD5E831B;
IL2CPP_EXTERN_C String_t* _stringLiteral5B4771ECD0D21D1246CB5851DB71541C1014C0E4;
IL2CPP_EXTERN_C String_t* _stringLiteral6DB1A489AAC23BDE10CC8953F40A10E404F6AEEF;
IL2CPP_EXTERN_C String_t* _stringLiteral6EC9BB6BFFC6813D0922B68630D8240FD2F05A7B;
IL2CPP_EXTERN_C String_t* _stringLiteral72E7318EA3265D7C6CCAE07CBF767DADD288A267;
IL2CPP_EXTERN_C String_t* _stringLiteral7C02239B26D2B2BD881ECB48C22D334EFB069C78;
IL2CPP_EXTERN_C String_t* _stringLiteral865EAEFB06C19932CCF3FC1E76B1900D48CB8279;
IL2CPP_EXTERN_C String_t* _stringLiteral87DA013322C535A557941E7326D33DC278F25DEE;
IL2CPP_EXTERN_C String_t* _stringLiteral8B5F20419E603451B4391ABC5029205CB4CBB858;
IL2CPP_EXTERN_C String_t* _stringLiteral8B6404E89F7993B03747A57B9F10D0BD9A31DCFF;
IL2CPP_EXTERN_C String_t* _stringLiteral8EFC1FB4B31116E9823D8C12E54D1FB4B8414455;
IL2CPP_EXTERN_C String_t* _stringLiteral8F7D7001F4DE3D7CD3BFC5BB7D95275C3EB58881;
IL2CPP_EXTERN_C String_t* _stringLiteral97018D67FED0C0FF852E5281DBF8C8F257213A41;
IL2CPP_EXTERN_C String_t* _stringLiteral98EDFEBCFE70A52621231D5932628644E13BAD58;
IL2CPP_EXTERN_C String_t* _stringLiteral9AE374142ACE97B2CB4325DC49D921B768F940F4;
IL2CPP_EXTERN_C String_t* _stringLiteral9CCA00E4A7A90645D430EB9DA62B53CD2B4A5E6F;
IL2CPP_EXTERN_C String_t* _stringLiteral9FA82863C7A6B0F0B394B57EED68ADED07832775;
IL2CPP_EXTERN_C String_t* _stringLiteralA2199373126ECBABCBFFF28E316BD3F98D454317;
IL2CPP_EXTERN_C String_t* _stringLiteralA2FA82CDC790441EA75DD736EE7B03848919C0C5;
IL2CPP_EXTERN_C String_t* _stringLiteralAABB007E42805B71E3DFFE2C74C1E325B964B369;
IL2CPP_EXTERN_C String_t* _stringLiteralB0C4D0C27994A10DFE6CD46CF413540D08DD0AB5;
IL2CPP_EXTERN_C String_t* _stringLiteralB155C3672C99C12CF77041B936DE4A3BD16005D0;
IL2CPP_EXTERN_C String_t* _stringLiteralBC6345647DB886DB24B0CF86E8FAC2E4BDA6485A;
IL2CPP_EXTERN_C String_t* _stringLiteralC124DE81C7950F509381621D910F2651A743813A;
IL2CPP_EXTERN_C String_t* _stringLiteralC7CABF4A5BC292D2EB5AA91FD0B53F19FAA623D5;
IL2CPP_EXTERN_C String_t* _stringLiteralEA4A3080201EE62CD522312D991C43597A41C3FA;
IL2CPP_EXTERN_C String_t* _stringLiteralEB720EEE4A86E783A0A1509524B380DD79D1E406;
IL2CPP_EXTERN_C String_t* _stringLiteralECC1C81DFD868EA6D6D83E5D4BD6031B306A77AE;
IL2CPP_EXTERN_C String_t* _stringLiteralF3D2337DBDFED067FE445B0D793FDB4E9710F4F3;
IL2CPP_EXTERN_C String_t* _stringLiteralF630824EE63853CB6CC737353D355D9AD03F54F1;
IL2CPP_EXTERN_C String_t* _stringLiteralFFEF3DBE279EE1F92E1E2E46F45BC18EBBF55A1A;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};
struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA  : public RuntimeObject
{
};
struct Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8  : public RuntimeObject
{
	int32_t ____inext;
	int32_t ____inextp;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____seedArray;
};
struct String_t  : public RuntimeObject
{
	int32_t ____stringLength;
	Il2CppChar ____firstChar;
};
struct UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC95F24D0C6E6B77389433852BB389F39C692926E  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	bool ___m_value;
};
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	uint8_t ___m_value;
};
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	float ___r;
	float ___g;
	float ___b;
	float ___a;
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	int32_t ___m_value;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D338_t005ADF437A0B0DC4CE5C9D1FD6926F6BB2D54FC2 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D338_t005ADF437A0B0DC4CE5C9D1FD6926F6BB2D54FC2__padding[338];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D98_t0CC00ECE230E886E2C474B2FAF659C888AA90AEB 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D98_t0CC00ECE230E886E2C474B2FAF659C888AA90AEB__padding[98];
	};
};
#pragma pack(pop, tp)
struct MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___FilePathsData;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	bool ___IsEditorOnly;
};
struct MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_pinvoke
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_com
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct KeyCode_t75B9ECCC26D858F55040DDFF9523681E996D17E9 
{
	int32_t ___value__;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr;
};
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	intptr_t ___value;
};
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___m_CancellationTokenSource;
};
struct ExitGame_t7535BD4AC5611EBE2045397500E11C4CF600C05F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
struct Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CoinSection;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___HeadsTailsButtons;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ContinueButton;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PlayerSection;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___OpponentSection;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___NextRoundButton;
	PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* ___phrases;
	bool ___coinFace;
	String_t* ___coin;
	bool ___playerCoin;
	uint8_t ___playerWins;
	uint8_t ___opponentWins;
	bool ___turn;
	bool ___insulterLosed;
	bool ___interactable;
	int32_t ___position;
	int32_t ___positionNext;
	int32_t ___positionPrevious;
	int32_t ___positionNextMore;
	int32_t ___positionPreviousMore;
	int32_t ___opponentPhrase;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textTurnInfo;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textPlayerWins;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textOpponentWins;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textInfo;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___RoundButton;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textSelected;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textNext;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textPrevious;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textNextMore;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textPreviousMore;
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___textOpponent;
};
struct GoMainMenu_t210EECA25DD14A02EBB82B97AB3202433AAE8D0B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
struct NewGame_tEDA469253EA2F19368D26AC497B1B3A436945546  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
struct PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PauseScreen;
};
struct PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___quote;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___answer;
};
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color;
	bool ___m_SkipLayoutUpdate;
	bool ___m_SkipMaterialUpdate;
	bool ___m_RaycastTarget;
	bool ___m_RaycastTargetCache;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding;
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform;
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer;
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas;
	bool ___m_VertsDirty;
	bool ___m_MaterialDirty;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback;
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback;
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh;
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs;
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner;
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField;
};
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	bool ___m_ShouldRecalculateStencil;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial;
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask;
	bool ___m_Maskable;
	bool ___m_IsMaskingGraphic;
	bool ___m_IncludeForMasking;
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged;
	bool ___m_ShouldRecalculate;
	int32_t ___m_StencilValue;
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners;
};
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData;
	String_t* ___m_Text;
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache;
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout;
	bool ___m_DisableFontTextureRebuiltCallback;
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts;
};
struct U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA_StaticFields
{
	__StaticArrayInitTypeSizeU3D98_t0CC00ECE230E886E2C474B2FAF659C888AA90AEB ___9EFE357290EC4790277098DF1446E15073D2461F8C8BC387C3171F68AE0697EB;
	__StaticArrayInitTypeSizeU3D338_t005ADF437A0B0DC4CE5C9D1FD6926F6BB2D54FC2 ___E5A4CAD744C5E475F71D0255D543D2077AA28BB76676E3DDE407EFC629AE60D3;
};
struct Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_StaticFields
{
	Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* ___s_globalRandom;
};
struct Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_ThreadStaticFields
{
	Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* ___t_threadRandom;
};
struct String_t_StaticFields
{
	String_t* ___Empty;
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	String_t* ___TrueString;
	String_t* ___FalseString;
};
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};



IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_mE304382DB9A6455C2A474C8F364C7387F37E9281 (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_coinFlip_mB18642929D1B944E14E35429EC1DFA48627BC394 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_randomPhraseOpponent_mF14C0844D9A87E21C05D49E0F20F5C7D3088CDE9 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Convert_ToString_mCFF0118493DD216E37512ABDB38A36962E7F82B1 (uint8_t ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E (String_t* ___0_sceneName, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random__ctor_m151183BD4F021499A98B9DE8502DAD4B12DD16AC (Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102 (int32_t ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_coinResult_m523813C0C50AC424844287A879EA009AFDE7FC4D (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9E3155FB84015C823606188F53B47CB44C444991 (String_t* ___0_str0, String_t* ___1_str1, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_changeTurn_m4E56C927C6DA4CCAB16F7C81E320A4CEB7EC1D6B (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhrasesList__ctor_m23583EDBE6A68D0D8AD9A61822CA74A2376920B2 (PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyUp_m9A962E395811A9901E7E05F267E198A533DBEF2F (int32_t ___0_key, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B (RuntimeArray* ___0_array, RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 ___1_fldHandle, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitGame_Exit_m821180E0BB5C461BC46444855744BB4334DA8073 (ExitGame_t7535BD4AC5611EBE2045397500E11C4CF600C05F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Application_tDB03BE91CDF0ACA614A5E0B67CFB77C44EB19B21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Application_tDB03BE91CDF0ACA614A5E0B67CFB77C44EB19B21_il2cpp_TypeInfo_var);
		Application_Quit_mE304382DB9A6455C2A474C8F364C7387F37E9281(NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExitGame__ctor_mF031F846CC7DE286155C56931B1D99E3B60E9151 (ExitGame_t7535BD4AC5611EBE2045397500E11C4CF600C05F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_Start_m54A868C511BA581D2409BA1EF45601337AEF4811 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF630824EE63853CB6CC737353D355D9AD03F54F1);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___textTurnInfo;
		VirtualActionInvoker1< String_t* >::Invoke(75, L_0, _stringLiteralF630824EE63853CB6CC737353D355D9AD03F54F1);
		Gameplay_coinFlip_mB18642929D1B944E14E35429EC1DFA48627BC394(__this, NULL);
		__this->___playerWins = (uint8_t)0;
		__this->___opponentWins = (uint8_t)0;
		Gameplay_randomPhraseOpponent_mF14C0844D9A87E21C05D49E0F20F5C7D3088CDE9(__this, NULL);
		__this->___insulterLosed = (bool)0;
		__this->___interactable = (bool)1;
		__this->___positionPreviousMore = 0;
		__this->___positionPrevious = 1;
		__this->___position = 2;
		__this->___positionNext = 3;
		__this->___positionNextMore = 4;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_Update_mD0E00A9987844E8D2FE851A6B59246951D28CF6B (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9AE374142ACE97B2CB4325DC49D921B768F940F4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0C4D0C27994A10DFE6CD46CF413540D08DD0AB5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC124DE81C7950F509381621D910F2651A743813A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFFEF3DBE279EE1F92E1E2E46F45BC18EBBF55A1A);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___textPlayerWins;
		uint8_t L_1 = __this->___playerWins;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		String_t* L_2;
		L_2 = Convert_ToString_mCFF0118493DD216E37512ABDB38A36962E7F82B1(L_1, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75, L_0, L_2);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___textOpponentWins;
		uint8_t L_4 = __this->___opponentWins;
		String_t* L_5;
		L_5 = Convert_ToString_mCFF0118493DD216E37512ABDB38A36962E7F82B1(L_4, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75, L_3, L_5);
		int32_t L_6 = __this->___position;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_7 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = L_7->___quote;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))
		{
			goto IL_0048;
		}
	}
	{
		__this->___position = 0;
	}

IL_0048:
	{
		int32_t L_9 = __this->___position;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_10 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = L_10->___quote;
		__this->___position = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_11)->max_length)), 1));
	}

IL_0066:
	{
		int32_t L_12 = __this->___positionNext;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_13 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = L_13->___quote;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))
		{
			goto IL_0082;
		}
	}
	{
		__this->___positionNext = 0;
	}

IL_0082:
	{
		int32_t L_15 = __this->___positionNext;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_00a0;
		}
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_16 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = L_16->___quote;
		__this->___positionNext = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_17)->max_length)), 1));
	}

IL_00a0:
	{
		int32_t L_18 = __this->___positionPrevious;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_19 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_19->___quote;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))
		{
			goto IL_00bc;
		}
	}
	{
		__this->___positionPrevious = 0;
	}

IL_00bc:
	{
		int32_t L_21 = __this->___positionPrevious;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_00da;
		}
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_22 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22->___quote;
		__this->___positionPrevious = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_23)->max_length)), 1));
	}

IL_00da:
	{
		int32_t L_24 = __this->___positionNextMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_25 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_25->___quote;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)(((RuntimeArray*)L_26)->max_length)))))
		{
			goto IL_00f6;
		}
	}
	{
		__this->___positionNextMore = 0;
	}

IL_00f6:
	{
		int32_t L_27 = __this->___positionNextMore;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0114;
		}
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_28 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28->___quote;
		__this->___positionNextMore = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_29)->max_length)), 1));
	}

IL_0114:
	{
		int32_t L_30 = __this->___positionPreviousMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_31 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_32 = L_31->___quote;
		if ((((int32_t)L_30) < ((int32_t)((int32_t)(((RuntimeArray*)L_32)->max_length)))))
		{
			goto IL_0130;
		}
	}
	{
		__this->___positionPreviousMore = 0;
	}

IL_0130:
	{
		int32_t L_33 = __this->___positionPreviousMore;
		if ((((int32_t)L_33) >= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_34 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_35 = L_34->___quote;
		__this->___positionPreviousMore = ((int32_t)il2cpp_codegen_subtract(((int32_t)(((RuntimeArray*)L_35)->max_length)), 1));
	}

IL_014e:
	{
		bool L_36 = __this->___turn;
		if (!L_36)
		{
			goto IL_0239;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_37 = __this->___textInfo;
		VirtualActionInvoker1< String_t* >::Invoke(75, L_37, _stringLiteralC124DE81C7950F509381621D910F2651A743813A);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_38 = __this->___textSelected;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_39 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_40 = L_39->___quote;
		int32_t L_41 = __this->___position;
		int32_t L_42 = L_41;
		String_t* L_43 = (L_40)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_42));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_38, L_43);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_44 = __this->___textNext;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_45 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_46 = L_45->___quote;
		int32_t L_47 = __this->___positionNext;
		int32_t L_48 = L_47;
		String_t* L_49 = (L_46)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_48));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_44, L_49);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_50 = __this->___textNextMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_51 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_52 = L_51->___quote;
		int32_t L_53 = __this->___positionNextMore;
		int32_t L_54 = L_53;
		String_t* L_55 = (L_52)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_54));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_50, L_55);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_56 = __this->___textPrevious;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_57 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_58 = L_57->___quote;
		int32_t L_59 = __this->___positionPrevious;
		int32_t L_60 = L_59;
		String_t* L_61 = (L_58)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_60));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_56, L_61);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_62 = __this->___textPreviousMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_63 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_64 = L_63->___quote;
		int32_t L_65 = __this->___positionPreviousMore;
		int32_t L_66 = L_65;
		String_t* L_67 = (L_64)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_66));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_62, L_67);
		bool L_68 = __this->___interactable;
		if (!L_68)
		{
			goto IL_0217;
		}
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_69 = __this->___textOpponent;
		VirtualActionInvoker1< String_t* >::Invoke(75, L_69, _stringLiteralFFEF3DBE279EE1F92E1E2E46F45BC18EBBF55A1A);
		goto IL_02f7;
	}

IL_0217:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_70 = __this->___textOpponent;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_71 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_72 = L_71->___answer;
		int32_t L_73 = __this->___opponentPhrase;
		int32_t L_74 = L_73;
		String_t* L_75 = (L_72)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_74));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_70, L_75);
		goto IL_02f7;
	}

IL_0239:
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_76 = __this->___textInfo;
		VirtualActionInvoker1< String_t* >::Invoke(75, L_76, _stringLiteral9AE374142ACE97B2CB4325DC49D921B768F940F4);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_77 = __this->___textSelected;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_78 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_79 = L_78->___answer;
		int32_t L_80 = __this->___position;
		int32_t L_81 = L_80;
		String_t* L_82 = (L_79)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_81));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_77, L_82);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_83 = __this->___textNext;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_84 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_85 = L_84->___answer;
		int32_t L_86 = __this->___positionNext;
		int32_t L_87 = L_86;
		String_t* L_88 = (L_85)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_87));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_83, L_88);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_89 = __this->___textNextMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_90 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_91 = L_90->___answer;
		int32_t L_92 = __this->___positionNextMore;
		int32_t L_93 = L_92;
		String_t* L_94 = (L_91)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_93));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_89, L_94);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_95 = __this->___textPrevious;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_96 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_97 = L_96->___answer;
		int32_t L_98 = __this->___positionPrevious;
		int32_t L_99 = L_98;
		String_t* L_100 = (L_97)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_99));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_95, L_100);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_101 = __this->___textPreviousMore;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_102 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102->___answer;
		int32_t L_104 = __this->___positionPreviousMore;
		int32_t L_105 = L_104;
		String_t* L_106 = (L_103)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_105));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_101, L_106);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_107 = __this->___textOpponent;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_108 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_109 = L_108->___quote;
		int32_t L_110 = __this->___opponentPhrase;
		int32_t L_111 = L_110;
		String_t* L_112 = (L_109)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_111));
		VirtualActionInvoker1< String_t* >::Invoke(75, L_107, L_112);
	}

IL_02f7:
	{
		uint8_t L_113 = __this->___playerWins;
		if ((((int32_t)L_113) >= ((int32_t)3)))
		{
			goto IL_0309;
		}
	}
	{
		uint8_t L_114 = __this->___opponentWins;
		if ((((int32_t)L_114) < ((int32_t)3)))
		{
			goto IL_032c;
		}
	}

IL_0309:
	{
		__this->___interactable = (bool)0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_115 = __this->___NextRoundButton;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_115, (bool)1, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_116 = __this->___RoundButton;
		VirtualActionInvoker1< String_t* >::Invoke(75, L_116, _stringLiteralB0C4D0C27994A10DFE6CD46CF413540D08DD0AB5);
	}

IL_032c:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_endGame_m85BBE536897965B7DF2561DA0A62BAB97BA423A0 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral33D88DF242845191F062C3179CFA2E9D7A39A4CE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB720EEE4A86E783A0A1509524B380DD79D1E406);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint8_t L_0 = __this->___playerWins;
		if ((((int32_t)L_0) < ((int32_t)3)))
		{
			goto IL_0013;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral33D88DF242845191F062C3179CFA2E9D7A39A4CE, NULL);
	}

IL_0013:
	{
		uint8_t L_1 = __this->___opponentWins;
		if ((((int32_t)L_1) < ((int32_t)3)))
		{
			goto IL_0026;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteralEB720EEE4A86E783A0A1509524B380DD79D1E406, NULL);
	}

IL_0026:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_coinFlip_mB18642929D1B944E14E35429EC1DFA48627BC394 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B4771ECD0D21D1246CB5851DB71541C1014C0E4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9CCA00E4A7A90645D430EB9DA62B53CD2B4A5E6F);
		s_Il2CppMethodInitialized = true;
	}
	Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* V_0 = NULL;
	{
		Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* L_0 = (Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8*)il2cpp_codegen_object_new(Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_il2cpp_TypeInfo_var);
		Random__ctor_m151183BD4F021499A98B9DE8502DAD4B12DD16AC(L_0, NULL);
		V_0 = L_0;
		Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* L_1 = V_0;
		int32_t L_2;
		L_2 = VirtualFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6, L_1, 0, 2);
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Convert_ToBoolean_mE5E29C04982F778600F57587CD121FEB55A31102(L_2, NULL);
		__this->___coinFace = L_3;
		bool L_4 = __this->___coinFace;
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		__this->___coin = _stringLiteral5B4771ECD0D21D1246CB5851DB71541C1014C0E4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___coin), (void*)_stringLiteral5B4771ECD0D21D1246CB5851DB71541C1014C0E4);
		return;
	}

IL_002d:
	{
		__this->___coin = _stringLiteral9CCA00E4A7A90645D430EB9DA62B53CD2B4A5E6F;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___coin), (void*)_stringLiteral9CCA00E4A7A90645D430EB9DA62B53CD2B4A5E6F);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_ChooseHeads_mD3F4720015F6E15DA52C67777CA96A5B9C331BC9 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		__this->___playerCoin = (bool)1;
		Gameplay_coinResult_m523813C0C50AC424844287A879EA009AFDE7FC4D(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_ChooseTails_mE8DF985CCE0DABC8F0AE5BF37FF33D6EB0FCABCF (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		__this->___playerCoin = (bool)0;
		Gameplay_coinResult_m523813C0C50AC424844287A879EA009AFDE7FC4D(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_coinResult_m523813C0C50AC424844287A879EA009AFDE7FC4D (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF3D2337DBDFED067FE445B0D793FDB4E9710F4F3);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___textTurnInfo;
		String_t* L_1 = __this->___coin;
		String_t* L_2;
		L_2 = String_Concat_m9E3155FB84015C823606188F53B47CB44C444991(_stringLiteralF3D2337DBDFED067FE445B0D793FDB4E9710F4F3, L_1, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75, L_0, L_2);
		bool L_3 = __this->___playerCoin;
		bool L_4 = __this->___coinFace;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0032;
		}
	}
	{
		__this->___turn = (bool)1;
		goto IL_0039;
	}

IL_0032:
	{
		__this->___turn = (bool)0;
	}

IL_0039:
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___HeadsTailsButtons;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_5, (bool)0, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = __this->___ContinueButton;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_6, (bool)1, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_startGame_m4B32279100EC5F8347ABF7C4BEE4BF941C626C41 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___CoinSection;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___PlayerSection;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___OpponentSection;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)1, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_randomPhraseOpponent_mF14C0844D9A87E21C05D49E0F20F5C7D3088CDE9 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* V_0 = NULL;
	{
		Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* L_0 = (Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8*)il2cpp_codegen_object_new(Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8_il2cpp_TypeInfo_var);
		Random__ctor_m151183BD4F021499A98B9DE8502DAD4B12DD16AC(L_0, NULL);
		V_0 = L_0;
		Random_t79716069EDE67D1D7734F60AE402D0CA3FB6B4C8* L_1 = V_0;
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_2 = __this->___phrases;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3 = L_2->___quote;
		int32_t L_4;
		L_4 = VirtualFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6, L_1, 0, ((int32_t)(((RuntimeArray*)L_3)->max_length)));
		__this->___opponentPhrase = L_4;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_nextOption_mD9C1DCB77FA504B7D9E3C0B7F97048FE4FBD5DC1 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___interactable;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_1 = __this->___positionPreviousMore;
		__this->___positionPreviousMore = ((int32_t)il2cpp_codegen_add(L_1, 1));
		int32_t L_2 = __this->___positionPrevious;
		__this->___positionPrevious = ((int32_t)il2cpp_codegen_add(L_2, 1));
		int32_t L_3 = __this->___position;
		__this->___position = ((int32_t)il2cpp_codegen_add(L_3, 1));
		int32_t L_4 = __this->___positionNext;
		__this->___positionNext = ((int32_t)il2cpp_codegen_add(L_4, 1));
		int32_t L_5 = __this->___positionNextMore;
		__this->___positionNextMore = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_004e:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_previousOption_m026B5E3932DE0327809FE455A3EC28A2879C01DC (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___interactable;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_1 = __this->___positionPreviousMore;
		__this->___positionPreviousMore = ((int32_t)il2cpp_codegen_subtract(L_1, 1));
		int32_t L_2 = __this->___positionPrevious;
		__this->___positionPrevious = ((int32_t)il2cpp_codegen_subtract(L_2, 1));
		int32_t L_3 = __this->___position;
		__this->___position = ((int32_t)il2cpp_codegen_subtract(L_3, 1));
		int32_t L_4 = __this->___positionNext;
		__this->___positionNext = ((int32_t)il2cpp_codegen_subtract(L_4, 1));
		int32_t L_5 = __this->___positionNextMore;
		__this->___positionNextMore = ((int32_t)il2cpp_codegen_subtract(L_5, 1));
	}

IL_004e:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_comparePhrases_m9D457357CD7B7A98CA485C7D43D80148F803E7C1 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___interactable;
		if (!L_0)
		{
			goto IL_00a4;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___NextRoundButton;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		bool L_2 = __this->___turn;
		if (!L_2)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_3 = __this->___position;
		int32_t L_4 = __this->___opponentPhrase;
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_004b;
		}
	}
	{
		uint8_t L_5 = __this->___opponentWins;
		__this->___opponentWins = (uint8_t)((int32_t)(uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, 1)));
		__this->___interactable = (bool)0;
		__this->___insulterLosed = (bool)1;
		return;
	}

IL_004b:
	{
		uint8_t L_6 = __this->___playerWins;
		__this->___playerWins = (uint8_t)((int32_t)(uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, 1)));
		__this->___interactable = (bool)0;
		return;
	}

IL_0062:
	{
		int32_t L_7 = __this->___position;
		int32_t L_8 = __this->___opponentPhrase;
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_008e;
		}
	}
	{
		uint8_t L_9 = __this->___playerWins;
		__this->___playerWins = (uint8_t)((int32_t)(uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, 1)));
		__this->___interactable = (bool)0;
		__this->___insulterLosed = (bool)1;
		return;
	}

IL_008e:
	{
		uint8_t L_10 = __this->___opponentWins;
		__this->___opponentWins = (uint8_t)((int32_t)(uint8_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, 1)));
		__this->___interactable = (bool)0;
	}

IL_00a4:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_changeTurn_m4E56C927C6DA4CCAB16F7C81E320A4CEB7EC1D6B (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___turn;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		__this->___turn = (bool)0;
		return;
	}

IL_0010:
	{
		__this->___turn = (bool)1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay_nextRound_m1DD666D1E5B2C7920D4A72B292BA34586F30362C (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___interactable;
		if (L_0)
		{
			goto IL_0036;
		}
	}
	{
		__this->___interactable = (bool)1;
		Gameplay_randomPhraseOpponent_mF14C0844D9A87E21C05D49E0F20F5C7D3088CDE9(__this, NULL);
		bool L_1 = __this->___insulterLosed;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Gameplay_changeTurn_m4E56C927C6DA4CCAB16F7C81E320A4CEB7EC1D6B(__this, NULL);
	}

IL_0023:
	{
		__this->___insulterLosed = (bool)0;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___NextRoundButton;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
	}

IL_0036:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gameplay__ctor_m8E09F5E281D54AAF3C24D4806CBB23AC07B1FD44 (Gameplay_tA5EED16EAA8DACAFCED215E081458F16B5CE8872* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* L_0 = (PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0*)il2cpp_codegen_object_new(PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0_il2cpp_TypeInfo_var);
		PhrasesList__ctor_m23583EDBE6A68D0D8AD9A61822CA74A2376920B2(L_0, NULL);
		__this->___phrases = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___phrases), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GoMainMenu_MainMenu_m6C18F860EDFD5F2F10EE484618D9E5DFB3552151 (GoMainMenu_t210EECA25DD14A02EBB82B97AB3202433AAE8D0B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GoMainMenu__ctor_m6A89B09FF843A3E3845C4F5412A61A1E22C37753 (GoMainMenu_t210EECA25DD14A02EBB82B97AB3202433AAE8D0B* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewGame_StartNewGame_m2B75F9BE431EB2E39303E46168CFA75B7451BD0D (NewGame_tEDA469253EA2F19368D26AC497B1B3A436945546* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NewGame__ctor_mC6903674A0BE368291E5EACA5CD7E1BDFD39CCDB (NewGame_tEDA469253EA2F19368D26AC497B1B3A436945546* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseScreenManager_LoadMainMenu_m378F524F25D5800A4FB6FD10ED7B6ED833BC63B2 (PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral000E6F488C4BFBAD929A9ED558662797D830E719, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseScreenManager_RestartGame_mB56878116B8FA3FC72F799005E2A28BE6F15FDA2 (PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(SceneManager_tA0EF56A88ACA4A15731AF7FDC10A869FA4C698FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_mBB3DBC1601A21F8F4E8A5D68FED30EA9412F218E(_stringLiteral079998E3393B6BDC1FAFFA63A54F724488AE5306, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseScreenManager_Update_m644382355B5EB779C0758A17E81503F47CE9CD3C (PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF* __this, const RuntimeMethod* method) 
{
	{
		bool L_0;
		L_0 = Input_GetKeyUp_m9A962E395811A9901E7E05F267E198A533DBEF2F(((int32_t)27), NULL);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___PauseScreen;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___PauseScreen;
		bool L_3;
		L_3 = GameObject_get_activeSelf_m4F3E5240E138B66AAA080EA30759A3D0517DA368(L_2, NULL);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), NULL);
	}

IL_0022:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PauseScreenManager__ctor_m10C54180E7553DFD25D04591DAE4444A279F48B4 (PauseScreenManager_t7EB616B39317676A23EB251FAA77D24ED1EFB8FF* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PhrasesList__ctor_m23583EDBE6A68D0D8AD9A61822CA74A2376920B2 (PhrasesList_tDD5B7A0CFF39EE7180B948CC6D3B4E6A4FB8BEF0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral024B70546D560BED2F9EA3257D6EC71771696892);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral02B8F8D294374EB8249103C858B7C63EBB98F2F7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0B40411E1B54CCCB28E3B083FCBBF3EE2033F42D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral149D2B7D3429AAA53F186CAD9F5D4ACD0C926F29);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26DD1B6869931EE786AA1943B3DE335F28FE59D6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38D5603050B3CFF94274D7A379692AA384E561E3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3A0B2A280DA0A7150C032A71F485BD835B6C26A8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F247E40A422873A44CCA84EA3443C5F50B7B9E1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral432B2BC02EC7CFA29A59818E6A3EC3D1132278B2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4BA3F3E82F8AD9EC15073A10072D1BE61E0AAE82);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4E16AB375A40CDDDEDE1C73E116812F8AD5E831B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6DB1A489AAC23BDE10CC8953F40A10E404F6AEEF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6EC9BB6BFFC6813D0922B68630D8240FD2F05A7B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral72E7318EA3265D7C6CCAE07CBF767DADD288A267);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7C02239B26D2B2BD881ECB48C22D334EFB069C78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral865EAEFB06C19932CCF3FC1E76B1900D48CB8279);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral87DA013322C535A557941E7326D33DC278F25DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8B5F20419E603451B4391ABC5029205CB4CBB858);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8B6404E89F7993B03747A57B9F10D0BD9A31DCFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8EFC1FB4B31116E9823D8C12E54D1FB4B8414455);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F7D7001F4DE3D7CD3BFC5BB7D95275C3EB58881);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97018D67FED0C0FF852E5281DBF8C8F257213A41);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98EDFEBCFE70A52621231D5932628644E13BAD58);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9FA82863C7A6B0F0B394B57EED68ADED07832775);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2199373126ECBABCBFFF28E316BD3F98D454317);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA2FA82CDC790441EA75DD736EE7B03848919C0C5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAABB007E42805B71E3DFFE2C74C1E325B964B369);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB155C3672C99C12CF77041B936DE4A3BD16005D0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBC6345647DB886DB24B0CF86E8FAC2E4BDA6485A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC7CABF4A5BC292D2EB5AA91FD0B53F19FAA623D5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA4A3080201EE62CD522312D991C43597A41C3FA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralECC1C81DFD868EA6D6D83E5D4BD6031B306A77AE);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->___quote = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___quote), (void*)L_0);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->___answer = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___answer), (void*)L_1);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = __this->___quote;
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral149D2B7D3429AAA53F186CAD9F5D4ACD0C926F29);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3 = __this->___quote;
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral9FA82863C7A6B0F0B394B57EED68ADED07832775);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = __this->___quote;
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral865EAEFB06C19932CCF3FC1E76B1900D48CB8279);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = __this->___quote;
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral38D5603050B3CFF94274D7A379692AA384E561E3);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = __this->___quote;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral8B6404E89F7993B03747A57B9F10D0BD9A31DCFF);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = __this->___quote;
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral8EFC1FB4B31116E9823D8C12E54D1FB4B8414455);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = __this->___quote;
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralA2199373126ECBABCBFFF28E316BD3F98D454317);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = __this->___quote;
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralAABB007E42805B71E3DFFE2C74C1E325B964B369);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = __this->___quote;
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteralBC6345647DB886DB24B0CF86E8FAC2E4BDA6485A);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = __this->___quote;
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteralA2FA82CDC790441EA75DD736EE7B03848919C0C5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = __this->___quote;
		(L_12)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral97018D67FED0C0FF852E5281DBF8C8F257213A41);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = __this->___quote;
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral98EDFEBCFE70A52621231D5932628644E13BAD58);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = __this->___quote;
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral4BA3F3E82F8AD9EC15073A10072D1BE61E0AAE82);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = __this->___quote;
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteralEA4A3080201EE62CD522312D991C43597A41C3FA);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = __this->___quote;
		(L_16)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral6EC9BB6BFFC6813D0922B68630D8240FD2F05A7B);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = __this->___quote;
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral024B70546D560BED2F9EA3257D6EC71771696892);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = __this->___answer;
		(L_18)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4E16AB375A40CDDDEDE1C73E116812F8AD5E831B);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = __this->___answer;
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3F247E40A422873A44CCA84EA3443C5F50B7B9E1);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = __this->___answer;
		(L_20)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteralC7CABF4A5BC292D2EB5AA91FD0B53F19FAA623D5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = __this->___answer;
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7C02239B26D2B2BD881ECB48C22D334EFB069C78);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = __this->___answer;
		(L_22)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral6DB1A489AAC23BDE10CC8953F40A10E404F6AEEF);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = __this->___answer;
		(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral87DA013322C535A557941E7326D33DC278F25DEE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = __this->___answer;
		(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral8B5F20419E603451B4391ABC5029205CB4CBB858);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = __this->___answer;
		(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral432B2BC02EC7CFA29A59818E6A3EC3D1132278B2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = __this->___answer;
		(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral0B40411E1B54CCCB28E3B083FCBBF3EE2033F42D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = __this->___answer;
		(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral72E7318EA3265D7C6CCAE07CBF767DADD288A267);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = __this->___answer;
		(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral26DD1B6869931EE786AA1943B3DE335F28FE59D6);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = __this->___answer;
		(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteralECC1C81DFD868EA6D6D83E5D4BD6031B306A77AE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = __this->___answer;
		(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteralB155C3672C99C12CF77041B936DE4A3BD16005D0);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = __this->___answer;
		(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral3A0B2A280DA0A7150C032A71F485BD835B6C26A8);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_32 = __this->___answer;
		(L_32)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral8F7D7001F4DE3D7CD3BFC5BB7D95275C3EB58881);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_33 = __this->___answer;
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral02B8F8D294374EB8249103C858B7C63EBB98F2F7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____9EFE357290EC4790277098DF1446E15073D2461F8C8BC387C3171F68AE0697EB_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____E5A4CAD744C5E475F71D0255D543D2077AA28BB76676E3DDE407EFC629AE60D3_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)338));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = L_0;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____E5A4CAD744C5E475F71D0255D543D2077AA28BB76676E3DDE407EFC629AE60D3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_1, L_2, NULL);
		(&V_0)->___FilePathsData = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___FilePathsData), (void*)L_1);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)98));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = L_3;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t0F5473E849A5A5185A9F4C5246F0C32816C49FCA____9EFE357290EC4790277098DF1446E15073D2461F8C8BC387C3171F68AE0697EB_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_4, L_5, NULL);
		(&V_0)->___TypesData = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___TypesData), (void*)L_4);
		(&V_0)->___TotalFiles = 6;
		(&V_0)->___TotalTypes = 6;
		(&V_0)->___IsEditorOnly = (bool)0;
		MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E L_6 = V_0;
		return L_6;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1 (UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC95F24D0C6E6B77389433852BB389F39C692926E* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_pinvoke(const MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E& unmarshaled, MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_pinvoke& marshaled)
{
	marshaled.___FilePathsData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___FilePathsData);
	marshaled.___TypesData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___TypesData);
	marshaled.___TotalTypes = unmarshaled.___TotalTypes;
	marshaled.___TotalFiles = unmarshaled.___TotalFiles;
	marshaled.___IsEditorOnly = static_cast<int32_t>(unmarshaled.___IsEditorOnly);
}
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_pinvoke_back(const MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_pinvoke& marshaled, MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___FilePathsData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___FilePathsData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData));
	unmarshaled.___TypesData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TypesData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData));
	int32_t unmarshaledTotalTypes_temp_2 = 0;
	unmarshaledTotalTypes_temp_2 = marshaled.___TotalTypes;
	unmarshaled.___TotalTypes = unmarshaledTotalTypes_temp_2;
	int32_t unmarshaledTotalFiles_temp_3 = 0;
	unmarshaledTotalFiles_temp_3 = marshaled.___TotalFiles;
	unmarshaled.___TotalFiles = unmarshaledTotalFiles_temp_3;
	bool unmarshaledIsEditorOnly_temp_4 = false;
	unmarshaledIsEditorOnly_temp_4 = static_cast<bool>(marshaled.___IsEditorOnly);
	unmarshaled.___IsEditorOnly = unmarshaledIsEditorOnly_temp_4;
}
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_pinvoke_cleanup(MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___FilePathsData);
	marshaled.___FilePathsData = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___TypesData);
	marshaled.___TypesData = NULL;
}
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_com(const MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E& unmarshaled, MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_com& marshaled)
{
	marshaled.___FilePathsData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___FilePathsData);
	marshaled.___TypesData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___TypesData);
	marshaled.___TotalTypes = unmarshaled.___TotalTypes;
	marshaled.___TotalFiles = unmarshaled.___TotalFiles;
	marshaled.___IsEditorOnly = static_cast<int32_t>(unmarshaled.___IsEditorOnly);
}
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_com_back(const MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_com& marshaled, MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___FilePathsData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___FilePathsData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData));
	unmarshaled.___TypesData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TypesData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData));
	int32_t unmarshaledTotalTypes_temp_2 = 0;
	unmarshaledTotalTypes_temp_2 = marshaled.___TotalTypes;
	unmarshaled.___TotalTypes = unmarshaledTotalTypes_temp_2;
	int32_t unmarshaledTotalFiles_temp_3 = 0;
	unmarshaledTotalFiles_temp_3 = marshaled.___TotalFiles;
	unmarshaled.___TotalFiles = unmarshaledTotalFiles_temp_3;
	bool unmarshaledIsEditorOnly_temp_4 = false;
	unmarshaledIsEditorOnly_temp_4 = static_cast<bool>(marshaled.___IsEditorOnly);
	unmarshaled.___IsEditorOnly = unmarshaledIsEditorOnly_temp_4;
}
IL2CPP_EXTERN_C void MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshal_com_cleanup(MonoScriptData_t8F50E352855B96FFFC1D9CB07EACC90C99D73A3E_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___FilePathsData);
	marshaled.___FilePathsData = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___TypesData);
	marshaled.___TypesData = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
