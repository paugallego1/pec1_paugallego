using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    // OBJETOS RELACIONADOS CON LA SELECCIÓN DE LA MONEDA Y GENERACIÓN DEL PRIMER TURNO
    public GameObject CoinSection; public GameObject HeadsTailsButtons; public GameObject ContinueButton;
    
    // OBJETOS RELACIONADOS CON EL GAMEPLAY
    public GameObject PlayerSection; public GameObject OpponentSection; public GameObject NextRoundButton;
    
    // LLAMADA AL SCRIP DONDE ESTAN TODAS LAS FRASES PARA LOS TEXTOS
    PhrasesList phrases = new PhrasesList();
    
    // DEFINICIÓN DE VARIABLES
    bool coinFace; string coin; bool playerCoin;
    
    const byte WIN = 3; byte playerWins ; byte opponentWins ;

    bool turn; bool insulterLosed; bool interactable;

    public int position; int positionNext; int positionPrevious; int positionNextMore; int positionPreviousMore;
    int opponentPhrase;
    
    // DEFICICIÓN DE TEXTOS DE LA UI
    public Text textTurnInfo;
    
    public Text textPlayerWins; public Text textOpponentWins;
    
    public Text textInfo; public Text RoundButton;
    
    public Text textSelected; public Text textNext; public Text textPrevious; public Text textNextMore; public Text textPreviousMore;

    public Text textOpponent;

    void Start()
    {
        // GENERADOR DE ALEATORIEDAD DEL TURNO INICIAL
        textTurnInfo.text = "¿CARA O CRUZ?";
        coinFlip();

        // INICIALIZAR NÚMERO DE ASALTOS GANADOS DE CADA PERSONAJE
        playerWins = 0;
        opponentWins = 0;

        // GENERAR FRASE ALEATORIA DEL OPONENTE E INICIALIZAR LOS ESTADOS DEL JUGADOR
        randomPhraseOpponent();
        insulterLosed = false;
        interactable = true;
        
        // INICIALIZAR POSICIONES DEL TEXTO DEL JUGADOR
        positionPreviousMore = 0;
        positionPrevious = 1;
        position = 2;
        positionNext = 3;
        positionNextMore = 4;

    }

    void Update()
    {
        // ACTUALIZACIÓN CONSTANTE DE LOS ASALTOS GANADOS DE LOS PERSONAJES
        textPlayerWins.text = Convert.ToString(playerWins);
        textOpponentWins.text = Convert.ToString(opponentWins);
        
        //textInfo.text = Convert.ToString(interactable);

        // ACTUALIZACIÖN CONSTANTE DE LA POSICION DE LOS TEXTOS DEL JUGADOR

        if (position >= phrases.quote.Length) {position = 0;}
        if (position < 0) {position = phrases.quote.Length - 1;}

        if (positionNext >= phrases.quote.Length) {positionNext = 0;}
        if (positionNext < 0) {positionNext = phrases.quote.Length - 1;}
        
        if (positionPrevious >= phrases.quote.Length) {positionPrevious = 0;}
        if (positionPrevious < 0) {positionPrevious = phrases.quote.Length - 1;}
        
        if (positionNextMore >= phrases.quote.Length) {positionNextMore = 0;}
        if (positionNextMore < 0) {positionNextMore = phrases.quote.Length - 1;}
        
        if (positionPreviousMore >= phrases.quote.Length) {positionPreviousMore = 0;}
        if (positionPreviousMore < 0) {positionPreviousMore = phrases.quote.Length - 1;}

        // ASIGNACION DE INSULTOS Y RESPUESTAS DEPENDIENDO DEL TURNO
        // 0 = TURNO DEL JUGADOR | 1 = TURNO DE OPONENTE
        if (turn == true)
        {
            textInfo.text = "Te toca insultar";

            textSelected.text = phrases.quote[position];
            textNext.text = phrases.quote[positionNext];
            textNextMore.text = phrases.quote[positionNextMore];
            textPrevious.text = phrases.quote[positionPrevious];
            textPreviousMore.text = phrases.quote[positionPreviousMore];

            //textOpponent.text = phrases.answer[opponentPhrase];
            if (interactable == true) {textOpponent.text = "...";}
            else {textOpponent.text = phrases.answer[opponentPhrase];}
        }
        else
        {
            textInfo.text = "Te toca responder";

            textSelected.text = phrases.answer[position];
            textNext.text = phrases.answer[positionNext];
            textNextMore.text = phrases.answer[positionNextMore];
            textPrevious.text = phrases.answer[positionPrevious];
            textPreviousMore.text = phrases.answer[positionPreviousMore];

            textOpponent.text = phrases.quote[opponentPhrase];
        }

        // ACTUALIZACIÓN CONSTANTE PARA COMPROBAR SI UNO DE LOS PERSONAJHES HA GANADO
        if (playerWins >= WIN || opponentWins >= WIN)
        {
            interactable = false;
            NextRoundButton.SetActive(true);
            RoundButton.text = "FINALIZAR JUEGO";
        }
    }

    public void endGame()
    {
        if (playerWins >= WIN)
        {
            SceneManager.LoadScene("EndWin");
        }
        if (opponentWins >= WIN)
        {
            SceneManager.LoadScene("EndLose");
        }
    }

    public void coinFlip()
    {
       // TRUE = HEADS || FALSE = TAILS
       System.Random randomTurn = new System.Random();
       coinFace = Convert.ToBoolean(randomTurn.Next(0, 2));

        if (coinFace == true) {coin = "cara";}
        else {coin = "cruz";}
    }

    // SELECCIONA PARA ELEGIR CARA
    public void ChooseHeads()
    {
        playerCoin = true;
        coinResult();
    }
    
    // SELECCIONA PARA ELEGIR CRUZ
    public void ChooseTails()
    {
        playerCoin = false;
        coinResult();
    }

    // MUESTRA EL RESULTADO DE LA MONEDA Y ASIGNA EL TURNO
    public void coinResult()
    {
        textTurnInfo.text = "Ha salido " + coin;
        
        // TURN TRUE = TURNO DEL JUGADOR || TURN FALSE = TURNO DE OPONENTE
        if (playerCoin == coinFace) {turn = true; }
        else {turn = false;}
        
        HeadsTailsButtons.SetActive(false);
        ContinueButton.SetActive(true);
    }

    // SELECCIONA PARA QUITAR EL MENU DE LA MONEDA Y EMPEZAR EL JUEGO
    public void startGame()
    {
        CoinSection.SetActive(false);
        PlayerSection.SetActive(true);
        OpponentSection.SetActive(true);
    }

    public void randomPhraseOpponent()
    {
        // GENERADOR DE ALEATORRIEDAD DE LAS FRASES DEL OPONENTE
        System.Random randomPhrase = new System.Random();
        opponentPhrase = (int)randomPhrase.Next(0,phrases.quote.Length);
    }

    // SELECCIONAR SIGUIENTE OPCIÓN DEL JUGADOR
    public void nextOption() {
        if (interactable == true)
        {
            positionPreviousMore++;
            positionPrevious++;
            position++;
            positionNext++;
            positionNextMore++;
        }
    }

    // SELECCIONAR OPCIÓN ANTERIOR DEL JUGADOR
    public void previousOption()
    {
        if (interactable == true)
        {
            positionPreviousMore--;
            positionPrevious--;
            position--;
            positionNext--;
            positionNextMore--;
        }
    }
    
    public void comparePhrases()
    {
        // COMPARA LOS INSULTOS Y LAS RESPUESTAS PARA QUE SEAN LOS ADECUADOS
        if (interactable == true)
        {
            NextRoundButton.SetActive(true);
            switch (turn)
            {
                case true:
                    if (position == opponentPhrase)
                    {
                        opponentWins++;
                        interactable = false;
                        insulterLosed = true;
                        break;
                    }
                    else
                    {
                        playerWins++;
                        interactable = false;
                        break;
                    }
                case false:
                    if (position == opponentPhrase)
                    {
                        playerWins++;
                        interactable = false;
                        insulterLosed = true;
                        break;
                    }
                    else
                    {
                        opponentWins++;
                        interactable = false;
                        break;
                    }
            }
        }
    }

    public void changeTurn()
    {
        // CAMBIA DE TURNO
        if (turn == true) {turn = false;}
        else {turn = true;}
    }
    
    public void nextRound()
    {
        // DA PIE A LA SIGUEINTE RONDA DESPUES DE COMPARAR LOS TEXTOS
        if (interactable == false)
        {
            interactable = true;
            randomPhraseOpponent();
            
            // EN CASO DE QUE EL "INSULTADOR" HAYA PERDIDO LA RONDA, PIERDE EL TURNO EN LA SIGUIENTE
            if (insulterLosed == true) {changeTurn();}
            
            insulterLosed = false;
            NextRoundButton.SetActive(false);
        }
    }
}
