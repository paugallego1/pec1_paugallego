using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PhrasesList : MonoBehaviour
{
    // LISTA DE FRASES DE TEXTO
    
    public string[] quote = new string[16];
    public string[] answer = new string[16];
    
    public PhrasesList()
    {
        // INSULTOS
        quote[0] = "¿Has dejado ya de usar pañales?";
        quote[1] = "¡No hay palabras para describir lo asqueroso que eres!";
        quote[2] = "¡He hablado con simios más educados que tu!";
        quote[3] = "¡Llevarás mi espada como si fueras un pincho moruno!";
        quote[4] = "¡Luchas como un ganadero!";
        quote[5] = "¡No pienso aguantar tu insolencia aquí sentado!";
        quote[6] = "¡Mi pañuelo limpiará tu sangre!";
        quote[7] = "¡Ha llegado tu HORA, palurdo de ocho patas!";
        quote[8] = "¡Una vez tuve un perro más listo que tu!";
        quote[9] = "¡Nadie me ha sacado sangre jamás, y nadie lo hará!";
        quote[10] = "¡Me das ganas de vomitar!";
        quote[11] = "¡Tienes los modales de un mendigo!";
        quote[12] = "¡He oído que eres un soplón despreciable!";
        quote[13] = "¡La gente cae a mis pies al verme llegar!";
        quote[14] = "¡Demasiado bobo para mi nivel de inteligencia!";
        quote[15] = "¡Obtuve esta cicatriz en una batalla a muerte!";

        // RESPUESTAS
        answer[0] = "¿Por qué? ¿Acaso querías pedir uno prestado?";
        answer[1] = "Sí que las hay, sólo que nunca las has aprendido.";
        answer[2] = "Me alegra que asistieras a tu reunión familiar diaria.";
        answer[3] = "Primero deberías dejar de usarla como un plumero.";
        answer[4] = "Qué apropiado, tú peleas como una vaca.";
        answer[5] = "Ya te están fastidiando otra vez las almorranas, ¿Eh?";
        answer[6] = "Ah, ¿Ya has obtenido ese trabajo de barrendero?";
        answer[7] = "Y yo tengo un SALUDO para ti, ¿Te enteras?";
        answer[8] = "Te habrá enseñado todo lo que sabes.";
        answer[9] = "¿TAN rápido corres?";
        answer[10] = "Me haces pensar que alguien ya lo ha hecho.";
        answer[11] = "Quería asegurarme de que estuvieras a gusto conmigo.";
        answer[12] = "Qué pena me da que nadie haya oído hablar de ti";
        answer[13] = "¿Incluso antes de que huelan tu aliento?";
        answer[14] = "Estaría acabado si la usases alguna vez.";
        answer[15] = "Espero que ya hayas aprendido a no tocarte la nariz.";
    }
}
